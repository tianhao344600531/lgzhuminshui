package com.example.common;
import java.util.ArrayList;
import java.util.List;

public final class UtilClass {

	public static List<String> getProvince(){
		List<String> provinceList = new ArrayList<String>();
		provinceList.add("千葉県");
		provinceList.add("東京都");
		provinceList.add("青森県");
		provinceList.add("秋田県");
		provinceList.add("山形県");
		provinceList.add("福島県");
		provinceList.add("岩手県");
		provinceList.add("茨城県");
		provinceList.add("群馬県");
		provinceList.add("埼玉県");
		provinceList.add("静岡県");
		provinceList.add("富山県");
		provinceList.add("石川県");
		provinceList.add("三重県");
		provinceList.add("愛知県");
		provinceList.add("滋賀県");
		provinceList.add("和歌山県");
		provinceList.add("兵庫県");
		provinceList.add("奈良県");
		provinceList.add("鳥取県");
		provinceList.add("福岡県");
		provinceList.add("熊本県");
		provinceList.add("大分県");
		provinceList.add("鹿児島県");
		provinceList.add("佐賀県");
		provinceList.add("京都府");
		provinceList.add("大阪府");
		provinceList.add("山口県");
		provinceList.add("宮崎県");
		provinceList.add("高知県");
		provinceList.add("長崎県");
		provinceList.add("北海道");

		return provinceList;
	}

	public static List<String> getTableHeader(){
		List<String> tableHeader = new ArrayList<String>();
		tableHeader.add("id");
		tableHeader.add("name");
		tableHeader.add("sex");
		tableHeader.add("age");
		tableHeader.add("area");
		tableHeader.add("type");
		tableHeader.add("money");

		return tableHeader;
	}
}
