package com.example.form;

import java.util.List;

public class SearchScreenForm {

	private String id;
	private String name;
	private String sex;
	private Integer age;
	private String area;
	private String type;
	private String money;
	private List<SearchScreenTableRow> searchScreenTables;
	private String[] provinceList;
	private boolean initFlag = false;

	public boolean isInitFlag() {
		return initFlag;
	}

	public void setInitFlag(boolean initFlag) {
		this.initFlag = initFlag;
	}

	public String[] getProvinceList() {
		return provinceList;
	}

	public List<SearchScreenTableRow> getSearchScreenTables() {
		return searchScreenTables;
	}

	public void setSearchScreenTables(List<SearchScreenTableRow> searchScreenTables) {
		this.searchScreenTables = searchScreenTables;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public void setProvinceList(String[] provinceList) {
		this.provinceList = provinceList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
