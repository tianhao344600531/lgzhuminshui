package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.EditService;
import com.example.form.EditScreenForm;

@Controller
public class EditScreenController {

	@Autowired
	private EditService editService;

	@RequestMapping("/edit")
	public ModelAndView init() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("editScreen");
		return modelAndView;
	}

	@RequestMapping("/extra")
	public ModelAndView extraInit() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("extra");
		return modelAndView;
	}

	@RequestMapping("/updateMoney")
	public ModelAndView updateMoney(EditScreenForm editScreenForm) {
		editService.updateUsersService(editScreenForm);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("detail");
		return modelAndView;
	}
//
//	@RequestMapping("/extraupdateMoney")
//	public ModelAndView extraupdateMoney(EditScreenForm editScreenForm) {
//		editService.updateUsersService(editScreenForm);
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("detail");
//		return modelAndView;
//	}
}
