package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.service.SearchService;
import com.example.form.DetailForm;

@Controller
public class SearchDetailController {

	@Autowired
	private SearchService searchService;


	@RequestMapping("/detail")

	public ModelAndView searchDetail(DetailForm detailForm) {

		ModelAndView modelAndView = new ModelAndView();
		DetailForm detailBack = searchService.searchForDetail(detailForm);
		modelAndView.addObject("DetailForm", detailBack);
		modelAndView.setViewName("detail");
		return modelAndView;
	}
}
