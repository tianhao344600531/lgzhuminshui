package com.example.demo.controller;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.example.demo.service.SearchService;
import com.example.form.SearchScreenForm;
import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class SearchHeaderController {

	@Autowired
	private SearchService searchService; 
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping("/")
	public ModelAndView init() {

		SearchScreenForm searchScreenForm = searchService.init();
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("SearchScreenForm", searchScreenForm);
		return modelAndView;
	}
	
	
	@RequestMapping("/search")
	public ModelAndView search(SearchScreenForm screenForm) {
		//call service
		SearchScreenForm screenFormBack = searchService.searchForHeader(screenForm);
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		modelAndView.addObject("SearchScreenForm", screenFormBack);
		httpSession.setAttribute("SearchScreenForm", screenFormBack);
		return modelAndView;
	}
	
	
//	@RequestMapping("/search")
//	public ModelAndView search(SearchScreenForm screenForm) {
//		//call service
//		SearchScreenForm screenFormBack = searchService.searchForHeader(screenForm);
//		
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("index");
//		modelAndView.addObject("SearchScreenForm", screenFormBack);
//		httpSession.setAttribute("SearchScreenForm", screenFormBack);
//		return modelAndView;
//	}
	
	@PostMapping("/download")
	public ResponseEntity downloadCsv() throws JsonProcessingException {
		
		HttpHeaders httpHeaders = new HttpHeaders();
	    final String CONTENT_DISPOSITION_FORMAT  = "attachment; filename=\"%s\"; filename*=UTF-8''%s";
	    String fileName = "test.csv";
	    String headerValue = String.format(CONTENT_DISPOSITION_FORMAT,fileName,UriUtils.encode(fileName, StandardCharsets.UTF_8.name()));
		httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, headerValue);
		String csvContent = null;
		if(httpSession.getAttribute("SearchScreenForm") != null ) {
			csvContent = searchService.getCsvContent((SearchScreenForm) httpSession.getAttribute("SearchScreenForm"));
		}
		
		return new ResponseEntity(csvContent.getBytes(), httpHeaders, HttpStatus.OK);
	}
}
