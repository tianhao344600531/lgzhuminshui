package com.example.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.Users;

@Mapper
public interface UsersMapper {

	@Select("select id,username,sex,age,area,usertype,money,userdate from users where area = #{area};")
	List<Users> findByState(@Param("area") String area);

	@Select("select id,username,sex,age,area,usertype,money,userdate from users where id = #{id};")
	List<Users> findById(@Param("id") String id);

	@Select("select id,username,sex,age,area,usertype,money,userdate from users where sex = #{sex};")
	List<Users> findBySex(@Param("sex") String sex);

	@Select("select id,username,sex,age,area,usertype,money,userdate from users where type = #{type};")
	List<Users> findByType(@Param("usertype") String type);

	@Select("select id,username,sex,age,area,usertype,money,userdate from users  where area = #{area} and sex = #{sex} and usertype = #{type};")
	List<Users> findByCondition
	(@Param("area") String area,@Param("sex") String sex,@Param("type") String type);

}
