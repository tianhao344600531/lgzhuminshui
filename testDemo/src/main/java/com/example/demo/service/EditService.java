package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Users;
import com.example.demo.repository.UsersRepository;
import com.example.form.EditScreenForm;


@Service
public class EditService {

	@Autowired
	private UsersRepository usersRepository;
	
	public void updateUsersService(EditScreenForm editScreenForm) {
		List<String> list = new ArrayList<String>();
		list.add("");
		List<Users> usersList = usersRepository.findAllById(list);
		System.out.println(usersList);
		Users users = usersList.get(0);
		users.setMoney(editScreenForm.getIncoming());
		usersRepository.save(users);
		
	}
}
