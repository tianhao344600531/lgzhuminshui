package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Users;
import com.example.demo.mapper.UsersMapper;

@Repository
public class UsersDaoService {

	@Autowired
	private UsersMapper usersMapper;

	public List<Users> getUsersByArea(String area){
		List<Users> users = usersMapper.findByState(area);
		return users;
	}

	public List<Users> getUsersById(String id){
		List<Users> users = usersMapper.findById(id);
		return users;
	}
	
	public List<Users> getUsersBySex(String sex){
		List<Users> users = usersMapper.findBySex(sex);
		return users;
	}

	
	public List<Users> getUsersByType(String type){
		List<Users> users = usersMapper.findByType(type);
		return users;
	}

	
	public List<Users> getUsersByCondition(String area, String sex, String type){
		List<Users> users = usersMapper.findByCondition(area, sex, type);
		return users;
	}


}
